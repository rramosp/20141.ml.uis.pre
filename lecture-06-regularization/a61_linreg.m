clear all; close all; clc

x = load('x-linreg.dat'); 
y = load('y-linreg.dat');

m = length(y);

% Add intercept term to x and polynomial features
x = [ones(m, 1), x, x.^2, x.^3, x.^4, x.^5];

lambda_list = [0.0 0.1 10.0];

for i = 1:length(lambda_list)

    lambda = lambda_list(i);
 
    % --- INSERT YOUR CODE HERE -------
    %     first to build L and then to compute theta using the closed form equation

    L =
    theta = 

    % ---------------------------------
    
    x_vals = (-1:0.05:1)';
    features = [ ones(size(x_vals)), x_vals, x_vals.^2, x_vals.^3, x_vals.^4, x_vals.^5 ];

    subplot (1,3,i)
    plot(x(:,2),y,'+', 'MarkerSize', 4)
    hold on
    plot(x_vals, features*theta, "--", 'LineWidth', 2)

end
