import numpy as np
from matplotlib.pyplot import *
from scipy.interpolate import *

def plot_boundary_with_data(classifier, data, labels, title=None, density=200j):
    plot_margin = 0.2
    # keep these just for plotting later
    (maxx, minx) = (np.max (data[:,0]), np.min (data[:,0])) 
    (maxy, miny) = (np.max (data[:,1]), np.min (data[:,1]))
    maxx += np.abs(maxx)*plot_margin
    minx -= np.abs(minx)*plot_margin
    maxy += np.abs(maxy)*plot_margin
    miny -= np.abs(miny)*plot_margin
    
    # grid datapoints
    gx,gy=np.mgrid[minx:maxx:density,miny:maxy:density]
    gpoints = np.vstack([gx.flatten(),gy.flatten()]).T
 
    # classify grid datapoints
    p = classifier.predict(gpoints)
    
    # transform p to have index of classification labels (not the labels themselves)
    p=np.where(np.tile(np.unique(p),(p.shape[0],1))-np.array([p]).T==0)[1]*1.0+2
    p[0]=0

    # plot classification boundary
    g = griddata(gpoints,p,(gx,gy),method="nearest")
    imshow(g.T, extent=(minx,maxx,miny,maxy), origin="lower", cmap="gray")
    
    # plot data
    # loop all classes
    color = 0
    colors = ["b", "r", "g", "c", "m", "y", "k", "w"];
    for l in np.unique(labels):
      # separate class
      d0 = data[labels==l]
      # plot class 0
      scatter(d0[:,0],d0[:,1],c=colors[color%len(colors)])
      if title!=None:
         suptitle(title)
      color=color+1  
  
