import numpy as np

def gradient_descent(f,dfx,dfy,init,step_size=0.1, min_delta=0.01,max_iters=100, show_progress=False, figure=None):

  [x,y] = np.array(init)*1.0

  (last_delta, iteration_number) = (min_delta+0.1,1)
  while ( ( last_delta>min_delta) and (iteration_number<max_iters)):
     dx = # your code here
     dy = # your code here
     x  = # your code here
     y  = # your code here
     last_delta = np.sqrt(dx**2+dy**2)
     if show_progress:
        print "i="+str(iteration_number)+" d="+str(last_delta)+" (x,y,z)="+str(x)+","+str(y)+","+str(f(x,y))
     if figure!=None:
        figure.scatter(x,y,f(x,y))
     iteration_number += 1

  return x,y,iteration_number


