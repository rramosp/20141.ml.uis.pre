from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
from gd import gradient_descent

def f1(x,y):
  return np.sin(x**2+y**2)

def df1x(x,y):
  return 2*x*np.cos(x**2+y**2)

def df1y(x,y):
  return 2*y*np.cos(x**2+y**2)

# --------------

def f2(x,y):
  return 1+(x**2+y**2)/5

def df2x(x,y):
  return x*2/5

def df2y(x,y):
  return y*2/5

# ---------------------
# plot gradient descent iterations for two variables functions
# args:
#    f: name of two variables functions. must be defined as "def fname(x,y):"
#    dfx: name of the partial derivative of function f with respect to the first variable
#    dfx: name of the partial derivative of function f with respect to the second variable
#    xrange, yrange: x/y ranges for the plot
#    l: step size for the gradient descent
#    max_iters: maximum number of iterations
#
# ---------------------
def plotgd(f,dfx,dfy, init, xrange=[-2,2], yrange=[-2,2], step_size=0.01, max_iters=100):
  fig = plt.figure()
  ax = fig.gca(projection='3d')
  mpoints=40.0
  X, Y = np.meshgrid(np.arange(xrange[0],xrange[1], (xrange[1]-xrange[0])/mpoints), np.arange(yrange[0],yrange[1], (yrange[1]-yrange[0])/mpoints))
  Z = f(X,Y)
  ax.plot_wireframe(X,Y,Z, color="0.4")
  ax.set_zlim(np.min(Z),np.max(Z))

  (last_step_size, iteration_number) = (1,1)
  [x,y] = np.array(init)*1.0
  ax.scatter(x,y,f(x,y), color='g', s=100)
  x,y,iters = gradient_descent(f,dfx,dfy,init=[x,y], figure=ax, step_size=step_size)
  ax.scatter(x,y,f(x,y), color='r', s=100)

plotgd(f1, df1x, df1y, init=(0,0), step_size=0.02)
plotgd(f1, df1x, df1y, init=(1.0,0), step_size=0.02)
plotgd(f1, df1x, df1y, init=(1.5,0), step_size=0.001)
plotgd(f1, df1x, df1y, xrange=(-3.5,3.5), yrange=(-3.5,3.5), init=(1.6,2.0), step_size=0.001)
plotgd(f2,df2x,df2y, init=(1,1), step_size=0.1)
plotgd(f2,df2x,df2y, init=(1,1), step_size=4)

plt.show()
