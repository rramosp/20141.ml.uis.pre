import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from sklearn.linear_model import *
from sklearn.preprocessing import *

def g(x):
    return 1/(1+np.exp(-x))

def plot_linlog_reg(x,y, plot_linreg=True, plot_logreg=True):
    sc = MinMaxScaler(); 
    x=sc.fit_transform(x) 
    y=sc.fit_transform(np.array([y]).T)[:,0]
    
    x1 = np.hstack((np.ones((x.shape[0],1)),np.array([x[:,0]]).T))
    (minx,maxx) = (np.min(x[:,0]), np.max(x[:,0]))
    y = y>0.5
    
    w_init = np.zeros(x1.shape[1])
    def linear_regression_cost (theta):
         return (np.sum(((x1.dot(theta))-y)**2))
    
    def log_regression_cost (theta):
         return (np.sum((g(x1.dot(theta))-y)**2))
    
    linear_regression_res = minimize(linear_regression_cost, w_init, method='BFGS')
    linear_regression_coefs = linear_regression_res.x
    
    log_regression_res = minimize(log_regression_cost, w_init, method='BFGS')
    log_regression_coefs = log_regression_res.x
    
    xp  = np.arange(minx,maxx,(maxx-minx)/500.0)
    xpi = np.hstack((np.ones((xp.shape[0],1)), np.array([xp]).T))
    yp_linear_regression = xpi.dot(linear_regression_coefs)
    yp_log_regression    = g(xpi.dot(log_regression_coefs))
    
    xp_linreg_cut = np.argmin((yp_linear_regression-0.5)**2)
    xp_logreg_cut = np.argmin((yp_log_regression-0.5)**2)
    
    ypredict_linear_regression = x1.dot(linear_regression_coefs)>0.5
    ypredict_log_regression    = g(x1.dot(log_regression_coefs))>0.5
        
    plt.scatter(x[:,0],y)

    if plot_linreg:
        plt.plot(xp, yp_linear_regression, "r")
        plt.plot(np.ones((2,1))*xp[xp_linreg_cut], np.array([0,1]).T, "r--")
        print "linreg cut = "+ str(xp[xp_linreg_cut])+" accuracy = "+str(sum(ypredict_linear_regression==y)/(y.shape[0]*1.0))

    if plot_logreg:
        plt.plot(xp, yp_log_regression, "b")
        plt.plot(np.ones((2,1))*xp[xp_logreg_cut], np.array([0,1]).T, "b--")
        print "logreg cut = "+ str(xp[xp_logreg_cut])+ " accuracy = "+str(sum(ypredict_log_regression==y)/(y.shape[0]*1.0))

    plt.plot(xp, np.ones(xp.shape[0])*0.5, "g--")
    

(n0,n1)=(30,30)

x0=np.random.random(n0)*0.5
x1=np.random.random(n1)*0.5+0.4
x = np.array([np.append(x0,x1)]).T
y = np.append(np.zeros(n0), np.ones(n1))
plt.subplot(2,2,1)
plot_linlog_reg(x,y, plot_logreg=False)
plt.subplot(2,2,3)
plot_linlog_reg(x,y, plot_linreg=False)

x0=np.random.random(n0)*0.15
x1=np.random.random(n1)*0.6+0.1
x = np.array([np.append(x0,x1)]).T
y = np.append(np.zeros(n0), np.ones(n1))
plt.subplot(2,2,2)
plot_linlog_reg(x,y, plot_logreg=False)
plt.subplot(2,2,4)
plot_linlog_reg(x,y, plot_linreg=False)

plt.show()

