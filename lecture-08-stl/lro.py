import numpy as np
from scipy.optimize import minimize

class LogisticRegressionO:
    
    def __init__ (self, map_features_function=None, lmbd=0.0):
        self.map_features_function = map_features_function
        self.lmbd = lmbd
  
    def __str__(self):
        if self.map_features_function == None:
           fname = "None"
        else:
           fname = self.map_features_function.__name__
        return self.__class__.__name__+" (lmbd="+str(self.lmbd)+" map_features_function="+fname+")"

    def fit(self, X, c):
        m = X.shape[1]
        
        # map features
        if self.map_features_function!=None:
            X = self.map_features_function(X)

        # add a column of ones to x matrix
        X = np.hstack((np.ones((X.shape[0],1)),X))

        # initialize weights with zeroes
        w_init = np.zeros(X.shape[1])
        
        # define cost function
        def lrcost (theta):
            f = np.ones(X.shape[1]); f[0]=0
            return (np.sum((self.g(X.dot(theta))-c)**2)+sum(self.lmbd*f*theta**2))
#            h=self.g(X.dot(theta))
#            return np.sum(-c*np.log(h)-(1-c)*np.log(1-h))+np.sum(self.lmbd*f*theta**2)
        
        # minimize parameters
        res = minimize(lrcost, w_init, method='BFGS')
#        res = minimize(lrcost, w_init, method='nelder-mead', options={'xtol': 1e-8} )
            
        self.theta = res.x
    
    def predict(self, X):
        if self.map_features_function!=None:
            X = self.map_features_function(X)
            
        # add 1 for bias
        xone = np.hstack((np.ones((X.shape[0],1)),X))
        
        # threshold on g
        return self.g(self.theta.dot(xone.T))>0.5
    
    def score(self, X, c):
        return sum(self.predict(X)==c)*1.0/X.shape[0]
    
    def g(self, x):
        # compute g = 1 / (1-e^(-w'x))
       return 1/(1+np.exp(-x))
