import numpy as np

def compute_basic(train_labels, predicted_labels):
  negative_labels_idx            = train_labels==0
  predicted_labels_for_negatives = predicted_labels[negative_labels_idx]
  positive_labels_idx            = train_labels==1
  predicted_labels_for_positives = predicted_labels[positive_labels_idx]

  p = np.sum(positive_labels_idx)
  tp = np.sum(predicted_labels_for_positives==1)
  fn = np.sum(predicted_labels_for_positives==0)

  n = np.sum(negative_labels_idx)
  tn = np.sum(predicted_labels_for_negatives==0)
  fp = np.sum(predicted_labels_for_negatives==1)

  return (p, tp, fn, n, tn, fp)


def compute_TPR(train_labels, predicted_labels):
  (p, tp, fn, n, tn, fp) = compute_basic(train_labels, predicted_labels)
  return 1.0*tp/p

def compute_FPR(train_labels, predicted_labels):
  (p, tp, fn, n, tn, fp) = compute_basic(train_labels, predicted_labels)
  return 1.0*fp/n

def compute_FNR(train_labels, predicted_labels):
  (p, tp, fn, n, tn, fp) = compute_basic(train_labels, predicted_labels)
  return 1.0*fn/p

def compute_TNR(train_labels, predicted_labels):
  (p, tp, fn, n, tn, fp) = compute_basic(train_labels, predicted_labels)
  return 1.0*tn/n

def compute_PPV(train_labels, predicted_labels):
  (p, tp, fn, n, tn, fp) = compute_basic(train_labels, predicted_labels)
  return 1.0*tp/(tp+fp)

def compute_NPV(train_labels, predicted_labels):
  (p, tp, fn, n, tn, fp) = compute_basic(train_labels, predicted_labels)
  return 1.0*tn/(tn+fn)

def compute_ACC(train_labels, predicted_labels):
  (p, tp, fn, n, tn, fp) = compute_basic(train_labels, predicted_labels)
  return 1.0*(tp+tn)/(p+n)

def compute_F1S(train_labels, predicted_labels):
  (p, tp, fn, n, tn, fp) = compute_basic(train_labels, predicted_labels)
  return 2.0*tp/(2.0*tp+fp+fn)


  

 
   
