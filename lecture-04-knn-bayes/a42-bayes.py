import numpy as np
from sklearn.datasets import *
from plotdata import *
from bayes import *

(d,c) = make_blobs(n_samples=500, n_features=2, centers=6)

n = NaiveBayesClassifier()
n.fit(d,c)
plot_boundary_with_data(n,d,c)
show()

