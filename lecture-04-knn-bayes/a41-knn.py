import sys
import numpy as np
from sklearn.datasets import *
from sklearn.neighbors import KNeighborsClassifier
from plotdata import *


k=3
n=50

(d,c) = make_moons(n_samples=n, noise=0.2)

kn=KNeighborsClassifier(n_neighbors=k)
kn.fit(d,c)
pred = kn.predict(d)

# -----------
# compute accuracy per class
# ---------
#
# INSERT YOUR CODE HERE
#  - compute and print out the global accuracy
#  - compute and print out the accuracy per class
#
# ---------

plot_boundary_with_data(kn,d,c)

show()

