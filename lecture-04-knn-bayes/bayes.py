import numpy as np

class NaiveBayesClassifier:

  def __init__ (self):
    self.means = dict()
    self.stdevs = dict()
    self.labels = []

  def gaus(self,x,m=0,s=1):
    if s==0:
      s=0.0001
    f1 = 1/(s*(2*np.pi)**0.5)
    f2 = np.exp(-((x-m)**2)/(2*s**2))
    return ( f1*f2 )

  def fit(self, d, c):
    # keep a list of labels
    self.labels = np.unique(c);
    for l in self.labels:
        # self.means[l] must be a vector with the means of
        # dataset columns for elements of class 'l'
        #
        # analogously for self.stdevs[l] for the standard
        # deviation
        #
        self.means[l]   =  *** YOUR CODE HERE ***
        self.stdevs[l]  =  *** YOUR CODE HERE ***

  def predict(self, X):
    # assume X is an m by n matrix (m datapoints with n dimensions)

    # STEP 1: build an m by c matrix (m datapoints for c classes)
    #         where each row i contains the probabilities of
    #         of datapoint i belonging to each class
    #
    # STEP 2: build a vector of m elements containing the
    #         most probable class for each datapoint
    #         note that in self.labels you will find the
    #         list of labels with which the classifier was
    #         trained

    return ** the vector resulting from step 29 **

